// 기본 이벤트 제거
window.addEventListener("wheel", (e) => {
    e.preventDefault();
}, {passive : false});

$(function() {
    // 스크롤시 section 이동
    const htmlElement = $("html");
    let pageNum = 1;
    let lastPageNum = $("section").length;

    htmlElement.animate({scrollTop: 0}, 300);

    $(window).on("wheel", (e) => {
        if (htmlElement.is(":animated")) return;

        if (e.originalEvent.deltaY > 0) {
            if (pageNum == lastPageNum) return;
            pageNum++;
        } else if (e.originalEvent.deltaY < 0) {
            if (pageNum == 1) return;
            pageNum--;
        }

        switch(pageNum) {
            case 1:
                $("nav ul li").removeClass("active");
                $("nav ul li:nth-child(1)").addClass("active");
                break;
            case 2:
                $("nav ul li").removeClass("active");
                $("nav ul li:nth-child(2)").addClass("active");
                break;
            case 3:
                $("nav ul li").removeClass("active");
                $("nav ul li:nth-child(3)").addClass("active");
                break;
            default:
                break;
        }

        let posTop = (pageNum - 1) * $(window).height();

        htmlElement.animate({scrollTop: posTop});
    });

    // nav click event
    $("nav ul li").on("click", (e) => { // 화살표 함수에서 this는 스스로를 가리키지 않는다. 대신 외부 컨텍스트의 this값이 적용된다.
        const navEl = $("nav ul li");
        let indexNum = $(e.currentTarget).index() + 1; // click 이벤트가 발생 했을 때 이벤트 객체 e의 currentTarget에 접근하면 click 된 요소를 조작할 수 있다.
        switch (indexNum) {
            case 1:
                htmlElement.animate({scrollTop: $("#secIntro").offset().top}, 300);
                break;
            case 2:
                htmlElement.animate({scrollTop: $("#secAbout").offset().top}, 300);
                break;
            case 3:
                htmlElement.animate({scrollTop: $("#secPortfolio").offset().top}, 300);
                break;
            default:
                break;
        }
        navEl.removeClass("active");
        $(e.currentTarget).addClass("active");
    });

    // popup open
    function popupOpen(num) {
        $(".box-popup[data-index-num="+num+"]").addClass("active");
        $("body").on("scroll touchmove mousewheel", (e) => {
            e.preventDefault();
            e.stopPropagation();
            return false;
        });
        $(".box-popup[data-index-num="+num+"] .popup-content").css("height", $(".box-popup[data-index-num="+num+"] .popup-wrapper").height() + 1);
    }
    $(".sec-portfolio .box-slide .item > a").on("click", (e) => {
        let num = $(e.currentTarget).attr("data-index-num");
        popupOpen(num);
    });

    // popup close
    $(".box-popup .popup-close").on("click", (e) => {
        $(e.currentTarget).parents(".box-popup").removeClass("active");
        $("body").off("scroll touchmove mousewheel");
        $(e.currentTarget).parents(".popup-wrapper").find(".popup-content").removeAttr("style");
    });
});

window.addEventListener("load", (event) => {
    // observer
    const boxElement = document.querySelectorAll(".observer");
    const options = {
        root: null,
        rootMargin: "0px",
        threshold: 0.5,
    }
    const handleIntersect = (entries, observer) => {
        entries.forEach((entry) => {
            if (entry.isIntersecting) {
                entry.target.classList.add("active");
            } else {
                entry.target.classList.remove("active");
            }
        });
    }
    const observer = new IntersectionObserver(handleIntersect, options);
    boxElement.forEach(el => observer.observe(el));

    // swiper
    const slideElement = new Swiper(".box-slide", {
        loop: false,
        slidesPerView: "auto",
        navigation: {
            nextEl: ".btn-next",
            prevEl: ".btn-prev",
        },
    });
});